/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author LucianoTadeu
 */
public class PontoXY extends Ponto2D {
    
    //public PontoXY() {
    public PontoXY() {
        //x = 0;
        //y = 0;
        //z = 0;
        super();
    }
    
    /**
     * Construtor que inicializa as coordenadas x, y e z
     * @param x
     * @param y
     */
    //public PontoXY(double x, double y) {
    public PontoXY(double x, double y) {
        //this.x = x;
        //this.y = y;
        //this.z = z;
        super(x, y, 0.0);
    }
    
    /* Método toString sobrecarregado */
    @Override
    public String toString() {
        return String.format("%s(%f,%f)", this.getNome(), this.getX(), this.getY());
    }
    
}
