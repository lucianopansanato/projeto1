package utfpr.ct.dainf.if62c.projeto;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto {
    private double x, y, z;

    /**
     * Construtor padrão
     */
    public Ponto() {
        x = 0;
        y = 0;
        z = 0;
    }
    
    /**
     * Construtor que inicializa as coordenadas x, y e z
     * @param x
     * @param y
     * @param z 
     */
    public Ponto(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }

    /* Getters/Setters */
    
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    /* Método toString sobrecarregado */
    @Override
    public String toString() {
        return String.format("%s(%f,%f,%f)", this.getNome(), x, y, z);
    }
    
    @Override
    public  boolean equals(Object o) {
        if(o == null) {
            return false;
        } else {
            if((o instanceof Ponto) && (((Ponto)o).getX()==this.x) && (((Ponto)o).getY()==this.y) && (((Ponto)o).getZ()==this.z)){

                return true;

            } else {

                return false;

            }
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + (int) (Double.doubleToLongBits(this.x) ^ (Double.doubleToLongBits(this.x) >>> 32));
        hash = 19 * hash + (int) (Double.doubleToLongBits(this.y) ^ (Double.doubleToLongBits(this.y) >>> 32));
        hash = 19 * hash + (int) (Double.doubleToLongBits(this.z) ^ (Double.doubleToLongBits(this.z) >>> 32));
        return hash;
    }
    
    public double dist(Ponto p) {
        return (Math.sqrt(Math.pow((p.getX() - this.x), 2) + Math.pow((p.getY() - this.y), 2) + Math.pow((p.getZ() - this.z), 2)));
    }
}
