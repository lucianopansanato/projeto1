/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author LucianoTadeu
 */
public class PontoYZ extends Ponto2D {
    
    //public PontoYZ() {
    public PontoYZ() {
        //x = 0;
        //y = 0;
        //z = 0;
        super();
    }
    
    /**
     * Construtor que inicializa as coordenadas x, y e z
     * @param y
     * @param z 
     */
    //public PontoYZ(double y, double z) {
    public PontoYZ(double y, double z) {
        //this.x = x;
        //this.y = y;
        //this.z = z;
        super(0.0, y, z);
    }
    
    /* Método toString sobrecarregado */
    @Override
    public String toString() {
        return String.format("%s(%f,%f)", this.getNome(), this.getY(), this.getZ());
    }

}
