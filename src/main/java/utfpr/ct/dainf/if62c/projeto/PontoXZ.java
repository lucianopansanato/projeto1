/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author LucianoTadeu
 */
public class PontoXZ extends Ponto2D {
    
    //public PontoXZ() {
    public PontoXZ() {
        //x = 0;
        //y = 0;
        //z = 0;
        super();
    }
    
    /**
     * Construtor que inicializa as coordenadas x, y e z
     * @param x
     * @param z 
     */
    //public PontoXZ(double x, double z) {
    public PontoXZ(double x, double z) {
        //this.x = x;
        //this.y = y;
        //this.z = z;
        super(x, 0.0, z);
    }
    
    /* Método toString sobrecarregado */
    @Override
    public String toString() {
        return String.format("%s(%f,%f)", this.getNome(), this.getX(), this.getZ());
    }

}
