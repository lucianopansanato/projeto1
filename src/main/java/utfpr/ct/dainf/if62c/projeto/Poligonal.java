/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author LucianoTadeu
 * @param <T> Tipo dos vértices
 */
public class Poligonal<T extends Ponto2D> {
    private T[] vertices;
    
    public Poligonal(T[] vertices) {
        if (vertices.length < 2) {
            throw new RuntimeException("Poligonal deve ter ao menos 2 vértices");
        } else {
            //int tam = vertices.length;
            //this.vertices = (T[]) new Object[tam];
            //System.arraycopy(vertices, 0, this.vertices, 0, tam);
            this.vertices = vertices;
        }
    }
    
    public int getN() {
        return vertices.length;
    }
    
    // Na classe Poligonal, implente o método genérico e público get(int) que 
    // deverá retornar o vértice na posição especificada. O primeiro vértice 
    // tem índice zero. Caso um índice inválido seja informado, este método 
    // deve retornar null.
    public T get(int indice) {
        if (indice < 0 || indice > (vertices.length - 1)) {
            return null;
        } else {
            return vertices[indice];
        }
    }
    
    // Na classe Poligonal, implente o método genérico e público set(int,T) 
    // que deverá armazenar o vértice especificado na posição informada. 
    // Caso o índice informado seja inválido, o método não deve fazer nada.
    public void set(int indice, T vert) {
        if (indice >= 0 && indice < vertices.length) {
            vertices[indice] = vert;
        }
    }
    
    // Na classe Poligonal, implente o método público getComprimento() que 
    // deverá retornar o comprimento da poligonal como um double. Lembre-se 
    // que esta é uma poligonal aberta. Sugestão: use o método dist, 
    // implementado no item 6.
    public double getComprimento() {
        double compr = 0.0;
        
        for (int i = 0; i < vertices.length - 1; i++) {
            compr = compr + vertices[i].dist(vertices[i+1]);
        }
        return compr;
    }
}
