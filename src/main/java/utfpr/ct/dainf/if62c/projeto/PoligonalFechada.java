/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

// Implemente a classe pública PoligonalFechada, derivada de Poligonal. 
// Implemente o construtor que recebe os vértices e sobrecarregue o método 
// getComprimento(), considerando que agora a poligonal é fechada. 
// Sugestão: observe que, em relação a uma poligonal aberta, basta 
// acrescentar a distância entre o primeiro e o último vértice para 
// obter o comprimento.
public class PoligonalFechada<T extends Ponto2D> extends Poligonal<T> {

    public PoligonalFechada(T[] vertices) {
        super(vertices);
    }
    
    @Override
    public double getComprimento() {
        double compr = super.getComprimento();
        int indice = super.getN() - 1;
        compr = compr + super.get(indice).dist(super.get(0));
        return compr;
    }
}
