
import utfpr.ct.dainf.if62c.projeto.PoligonalFechada;
import utfpr.ct.dainf.if62c.projeto.PontoXZ;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author 
 */
public class Projeto1 {

    public static void main(String[] args) {
        /*
        Ponto p1 = new Ponto(2.0, 1.0, 1.0);
        System.out.println(p1);
        Ponto p2 = new Ponto(4.0, 3.0, 2.0);
        System.out.println(p2);
        Ponto p3 = new Ponto(2.5, 5.5, 7.0);
        System.out.println(p3);
        System.out.println(p1.equals(p2));
        System.out.println(p1.equals(p3));
        Ponto p4 = null;
        System.out.println(p1.equals(p4));
        System.out.println(p1.dist(p2));
        */
        
        // Instancie uma poligonal fechada no plano XZ com os vértices de 
        // coordenadas (x, z), (-3,2), (-3,6) e (0,2), calcule  e exiba o 
        // comprimento. O programa deve exibir uma mensagem semelahante 
        // a "Comprimento da poligonal = 123,456".
        PontoXZ p1 = new PontoXZ(-3.0, 2.0);
        PontoXZ p2 = new PontoXZ(-3.0, 6.0);
        PontoXZ p3 = new PontoXZ(0.0, 2.0);

        //System.out.println(p1);
        //System.out.println(p2);
        //System.out.println(p3);
        //System.out.println(p1.dist(p2));
        //System.out.println(p2.dist(p3));
        //System.out.println(p3.dist(p1));
        //System.out.println();
        
        try {
            PontoXZ[] verts = {p1, p2, p3};
            PoligonalFechada<PontoXZ> pol = new PoligonalFechada<>(verts);
            System.out.println(String.format("Comprimento da poligonal = %f)", pol.getComprimento()));
        } catch (Exception e) {
            System.out.println("Ocorreu um erro inesperado: "
            + e.getLocalizedMessage());
        }
    }
}
